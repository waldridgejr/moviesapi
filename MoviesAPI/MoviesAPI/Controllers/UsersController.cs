﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.Models;
using MoviesAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// GET /api/users
        /// </summary>
        /// <returns> all users </returns>
        [HttpGet]
        public ActionResult<List<User>> Get()
        {
            return _userService.Get();
        }

        /// <summary>
        /// GET /api/users/{user ObjectId}
        /// </summary>
        /// <returns> user with given Id </returns>
        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public ActionResult<User> Get(string id)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        /// <summary>
        /// GET /api/users/{user ObjectId}
        /// </summary>
        /// <returns> all users with given first name </returns>
        [HttpGet("firstname/{FirstName}", Name = "GetUserFirstName")]
        public ActionResult<List<User>> GetFirstName(string firstName)
        {
            var user = _userService.GetFirstName(firstName);

            if (user.Count == 0)
            {
                return NotFound();
            }

            return user;
        }

        /// <summary>
        /// GET /api/users/{user ObjectId}
        /// </summary>
        /// <returns> all users with given last name </returns>
        [HttpGet("lastname/{LastName}", Name = "GetUserLastName")]
        public ActionResult<List<User>> GetLastName(string lastName)
        {
            var user = _userService.GetLastName(lastName);

            if (user.Count == 0)
            {
                return NotFound();
            }

            return user;
        }

        /// <summary>
        /// GET /api/users/{user ObjectId}
        /// </summary>
        /// <returns> all users with given age </returns>
        [HttpGet("age/{Age:int}", Name = "GetUserAge")]
        public ActionResult<List<User>> GetAge(int age)
        {
            var user = _userService.GetAge(age);

            if (user.Count == 0)
            {
                return NotFound();
            }

            return user;
        }

        /// <summary>
        /// POST /api/users
        /// </summary>
        /// <returns> new user inserted into collection </returns>
        [HttpPost]
        public ActionResult<User> Post(User user)
        {
            _userService.Post(user);

            return CreatedAtRoute("GetUser", new { id = user.Id.ToString() }, user);
        }

        /// <summary>
        /// PUT /api/users/{user ObjectId}
        /// </summary>
        /// <returns> replaces user at given Id with new user </returns>
        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, User userIn)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            _userService.Put(id, userIn);

            return NoContent();
        }

        /// <summary>
        /// PATCH /api/user/{user ObjectId}
        /// </summary>
        /// <returns> updates user at given Id </returns>
        [HttpPatch("{id:length(24)}")]
        public IActionResult Patch(string id, User userIn)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            _userService.Patch(id, userIn);

            return NoContent();
        }

        /// <summary>
        /// DELETE /api/users/{user ObjectId}
        /// </summary>
        /// <returns> deletes user at given Id </returns>
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            _userService.Remove(user.Id);

            return NoContent();
        }
    }
}