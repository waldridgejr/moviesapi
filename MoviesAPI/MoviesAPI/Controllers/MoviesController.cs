﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using MoviesAPI.Models;
using MoviesAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieService _movieService;

        public MoviesController(MovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// GET /api/movies
        /// </summary>
        /// <returns> all movies </returns>
        [HttpGet]
        public ActionResult<List<Movie>> Get()
        { 
            return _movieService.Get();
        }

        /// <summary>
        /// GET /api/movies/{movie ObjectId}
        /// </summary>
        /// <returns> movie with matching Id </returns>
        [Route("{id:length(24)}", Name = "GetMovie")]
        public ActionResult<Movie> Get(string id)
        {
            var movie = _movieService.Get(id);

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        /// <summary>
        /// GET /api/movies/{movie title}
        /// </summary>
        /// <returns> movies with matching titles </returns>
        [Route("title/{title}", Name = "GetMovieTitle")]
        public ActionResult<List<Movie>> GetTitle(string title)
        {
            var movies = _movieService.GetTitle(title);

            if (movies.Count == 0)
            {
                return NotFound();
            }

            return movies;
        }

        /// <summary>
        /// GET /api/movies/{movie release year}
        /// </summary>
        /// <returns> movies with matching release years </returns>
        [Route("year/{releaseYear:int}", Name = "GetMovieReleaseYear")]
        public ActionResult<List<Movie>> GetReleaseYear(int releaseYear)
        {
            var movies = _movieService.GetReleaseYear(releaseYear);

            if (movies.Count == 0)
            {
                return NotFound();
            }

            return movies;
        }

        /// <summary>
        /// POST /api/movies
        /// </summary>
        /// <returns> new movie inserted into collection </returns>
        [HttpPost]
        public ActionResult<Movie> Post(Movie movie)
        {
            _movieService.Post(movie);

            return CreatedAtRoute("GetMovie", new { id = movie.Id.ToString() }, movie);
        }

        /// <summary>
        /// PUT /api/movies/{movie ObjectId}
        /// </summary>
        /// <returns> replaces the movie at given Id with new movie </returns>
        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, Movie movieIn)
        {
            var movie = _movieService.Get(id);

            if (movie == null)
            {
                return NotFound();
            }

            _movieService.Put(id, movieIn);

            return NoContent();
        }

        /// <summary>
        /// PATCH /api/movies/{movie ObjectId}
        /// </summary>
        /// <returns> updates movie at given Id </returns>
        [HttpPatch("{id:length(24)}")]
        public IActionResult Patch(string id, Movie movieIn)
        {
            var movie = _movieService.Get(id);

            if (movie == null)
            {
                return NotFound();
            }

            _movieService.Patch(id, movieIn);

            return NoContent();
        }

        /// <summary>
        /// DELETE /api/movies/{movie ObjectId}
        /// </summary>
        /// <returns> deletes movie at given Id </returns>
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var movie = _movieService.Get(id);

            if (movie == null)
            {
                return NotFound();
            }

            _movieService.Remove(movie.Id);

            return NoContent();
        }
    }
}