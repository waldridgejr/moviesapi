﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.Models;
using MoviesAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly ReviewService _reviewService;

        public ReviewsController(ReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        /// <summary>
        /// GET /api/reviews
        /// </summary>
        /// <returns> all reviews </returns>
        [HttpGet]
        public ActionResult<List<Review>> Get()
        { 
            return _reviewService.Get();
        }

        /// <summary>
        /// GET /api/reviews/{review ObjectId}
        /// </summary>
        /// <returns> review with given Id </returns>
        [HttpGet("{id:length(24)}", Name = "GetReview")]
        public ActionResult<Review> Get(string id)
        {
            var review = _reviewService.Get(id);

            if (review == null)
            {
                return NotFound();
            }

            return review;
        }

        /// <summary>
        /// GET /api/reviews/{user ObjectId}
        /// </summary>
        /// <returns> review with given userId </returns>
        [HttpGet("user/{userId:length(24)}", Name = "GetUserReview")]
        public ActionResult<Review> GetUserReview(string userId)
        {
            var review = _reviewService.GetUserReview(userId);

            if (review == null)
            {
                return NotFound();
            }

            return review;
        }

        /// <summary>
        /// GET /api/reviews/{movie ObjectId}
        /// </summary>
        /// <returns> review with given userId </returns>
        [HttpGet("movie/{movieId:length(24)}", Name = "GetMovieReview")]
        public ActionResult<Review> GetMovieReview(string movieId)
        {
            var review = _reviewService.GetMovieReview(movieId);

            if (review == null)
            {
                return NotFound();
            }

            return review;
        }

        /// <summary>
        /// POST /api/reviews
        /// </summary>
        /// <returns> new review inserted into collection </returns>
        [HttpPost]
        public ActionResult<Review> Post(Review review)
        {
            _reviewService.Post(review);

            return CreatedAtRoute("GetReview", new { id = review.Id.ToString() }, review);
        }

        /// <summary>
        /// PUT /api/reviews/{review ObjectId}
        /// </summary>
        /// <returns> replaces the review with given Id </returns>
        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, Review reviewIn)
        {
            var review = _reviewService.Get(id);

            if (review == null)
            {
                return NotFound();
            }

            _reviewService.Put(id, reviewIn);

            return NoContent();
        }

        /// <summary>
        /// PATCH /api/reviews/{review ObjectId}
        /// </summary>
        /// <returns> updates review at given Id </returns>
        [HttpPatch("{id:length(24)}")]
        public IActionResult Patch(string id, Review reviewIn)
        {
            var review = _reviewService.Get(id);

            if (review == null)
            {
                return NotFound();
            }

            _reviewService.Patch(id, reviewIn);

            return NoContent();
        }

        /// <summary>
        /// DELETE /api/reviews/{review ObjectId}
        /// </summary>
        /// <returns> deletes review at given Id </returns>
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var review = _reviewService.Get(id);

            if (review == null)
            {
                return NotFound();
            }

            _reviewService.Remove(review.Id);

            return NoContent();
        }
    }
}