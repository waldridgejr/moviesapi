using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MoviesAPI.Models;
using MoviesAPI.Services;
using ReviewsAPI.Models;

namespace MoviesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //start MovieService
            services.Configure<MoviesDatabaseSettings>(
                 Configuration.GetSection(nameof(MoviesDatabaseSettings)));
            services.AddSingleton<IMoviesDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<MoviesDatabaseSettings>>().Value);
            services.AddSingleton<MovieService>();

            //start UserService
            services.Configure<UsersDatabaseSettings>(
                 Configuration.GetSection(nameof(UsersDatabaseSettings)));
            services.AddSingleton<IUsersDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<UsersDatabaseSettings>>().Value);
            services.AddSingleton<UserService>();

            //start ReviewService
            services.Configure<ReviewsDatabaseSettings>(
                 Configuration.GetSection(nameof(ReviewsDatabaseSettings)));
            services.AddSingleton<IReviewsDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<ReviewsDatabaseSettings>>().Value);
            services.AddSingleton<ReviewService>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
