﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MoviesAPI.Models
{
    public class Review
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string userId { get; set; }

        [BsonElement("User First Name")]
        public string userFirstName { get; set; }

        [BsonElement("User Last Name")]
        public string userLastName { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string movieId { get; set; }

        [BsonElement("Movie Title")]
        public string movieTitle { get; set; }

        [BsonElement("Review Body")]
        public string body { get; set; }
    }
}