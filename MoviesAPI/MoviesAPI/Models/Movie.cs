﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MoviesAPI.Models
{
    public class Movie
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Title { get; set; }

        [BsonElement("Release Year")]
        public int ReleaseYear { get; set; }

        [BsonElement("Average Rating")]
        public double AverageRating { get; set; }

        public string Description { get; set; }
    }
}