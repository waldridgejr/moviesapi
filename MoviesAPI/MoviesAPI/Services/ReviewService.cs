﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.Models;
using MongoDB.Driver;
using ReviewsAPI.Models;

namespace MoviesAPI.Services
{
    public class ReviewService
    {
        private readonly IMongoCollection<Review> _reviews;

        public ReviewService(IReviewsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _reviews = database.GetCollection<Review>(settings.ReviewsCollectionName);
        }

        public List<Review> Get()
        { 
            return _reviews.Find(review => true).ToList(); 
        }

        public Review Get(string id)
        { 
            return _reviews.Find<Review>(review => review.Id == id).FirstOrDefault();
        }

        public Review GetUserReview(string userId)
        {
            return _reviews.Find<Review>(review => review.userId == userId).FirstOrDefault();
        }

        public Review GetMovieReview(string movieId)
        {
            return _reviews.Find<Review>(review => review.movieId == movieId).FirstOrDefault();
        }

        public Review Post(Review review)
        {
            _reviews.InsertOne(review);
            return review;
        }

        public void Put(string id, Review reviewIn)
        {
            _reviews.ReplaceOne(review => review.Id == id, reviewIn);
        }

        public void Patch(string id, Review reviewIn)
        {
            var update = Builders<Review>.Update
                .Set("userId", reviewIn.userId)
                .Set("User First Name", reviewIn.userFirstName)
                .Set("User Last Name", reviewIn.userLastName)
                .Set("movieId", reviewIn.movieId)
                .Set("Movie Title", reviewIn.movieTitle)
                .Set("Review Body", reviewIn.body);

            _reviews.UpdateOne(review => review.Id == id, update);
        }

        public void Remove(Review reviewIn)
        {
            _reviews.DeleteOne(review => review.Id == reviewIn.Id);
        }

        public void Remove(string id)
        {
            _reviews.DeleteOne(review => review.Id == id);
        }
    }
}
