﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.Models;
using MongoDB.Driver;

namespace MoviesAPI.Services
{
    public class MovieService
    {
        private readonly IMongoCollection<Movie> _movies;

        public MovieService(IMoviesDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _movies = database.GetCollection<Movie>(settings.MoviesCollectionName);
        }

        public List<Movie> Get()
        {
            return _movies.Find(movie => true).ToList();
        }

        public Movie Get(string id)
        {
            return _movies.Find<Movie>(movie => movie.Id == id).FirstOrDefault();
        }

        public List<Movie> GetTitle(string title)
        {
            return _movies.Find<Movie>(movie => movie.Title == title).ToList();
        }
        public List<Movie> GetReleaseYear(int releaseYear)
        {
            return _movies.Find<Movie>(movie => movie.ReleaseYear == releaseYear).ToList();
        }

        public Movie Post(Movie movie)
        {
            _movies.InsertOne(movie);
            return movie;
        }

        public void Put(string id, Movie movieIn)
        {
            _movies.ReplaceOne(movie => movie.Id == id, movieIn);
        }

        public void Patch(string id, Movie movieIn)
        {
            var update = Builders<Movie>.Update
                .Set("Title", movieIn.Title)
                .Set("Release Year", movieIn.ReleaseYear)
                .Set("Average Rating", movieIn.AverageRating)
                .Set("Description", movieIn.Description);

            _movies.UpdateOne(movie => movie.Id == id, update);
        }

        public void Remove(Movie movieIn)
        {
            _movies.DeleteOne(movie => movie.Id == movieIn.Id);
        }

        public void Remove(string id)
        {
            _movies.DeleteOne(movie => movie.Id == id);
        }
    }
}
