﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesAPI.Models;
using MongoDB.Driver;

namespace MoviesAPI.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> _users;

        public UserService(IUsersDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public List<User> Get()
        { 
            return _users.Find(user => true).ToList();
        }

        public User Get(string id)
        { 
            return _users.Find<User>(user => user.Id == id).FirstOrDefault();
        }

        public List<User> GetFirstName(string firstName)
        {
            return _users.Find<User>(user => user.FirstName == firstName).ToList();
        }

        public List<User> GetLastName(string lastName)
        {
            return _users.Find<User>(user => user.LastName == lastName).ToList();
        }

        public List<User> GetAge(int age)
        {
            return _users.Find<User>(user => user.Age == age).ToList();
        }

        public User Post(User user)
        {
            _users.InsertOne(user);
            return user;
        }

        public void Put(string id, User userIn)
        {
            _users.ReplaceOne(user => user.Id == id, userIn);
        }

        public void Patch(string id, User userIn)
        {
            var update = Builders<User>.Update
                .Set("First Name", userIn.FirstName)
                .Set("Last Name", userIn.LastName)
                .Set("Age", userIn.Age);

            _users.UpdateOne(review => review.Id == id, update);
        }

        public void Remove(User userIn)
        {
            _users.DeleteOne(user => user.Id == userIn.Id);
        }
        public void Remove(string id)
        {
            _users.DeleteOne(user => user.Id == id);
        }
    }
}
